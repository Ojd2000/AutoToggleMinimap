# AutoToggleMinimap

Visual Studio Code's minimap is a useful and powerful tool when it comes to code navigating, but sometimes it is useless and just takes some space from the editor.

AutoToggleMinimap is an extension for VSCode that hides the minimap when all documents are short enough to be completely visible in the editor,
and re-enables it automatically when it is no longer the case.

## Features

The minimap is enabled when at least a visibile text editor is displaying a document longer than itself, thus it is showing a scrollbar.
Otherwise, it is disabled.

To toggle it, this extension uses the `editor.minimap.enabled` option in a workspace-level `settings.json` file, that is modified accordingly.

A workspace needs to be opened in order for this extension to work. It won't be activated if only single files are opened in VSCode.

## Known Issues

- At start, it seems that, if a document was already open in the workspace, the extension is not automatically triggered. Maybe some events have not been considered.

## Release Notes

See [CHANGELOG.md](./CHANGELOG.md).

## License

Licensed under the EUPL-1.2-or-later.
