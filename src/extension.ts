// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';

let minimapOption = 'editor.minimap.enabled';
let initialMinimapOption: boolean | undefined = undefined;

/**
 * Enable or disable minimap
 * @param val Whether it should be enabled.
 * If undefined, the option gets eliminated.
 * If null, it is deducted from visible ranges.
 * @returns 
 */
function updateMinimap(val: boolean | undefined | null) {
    // TODO: if active file is settings.json, do nothing

    // This extension will not modify global settings, therefore
    // it needs to check if current workspace exists on disk
    if (vscode.workspace.workspaceFolders === undefined) {
        return;
    }

    if (val === null) {
        let editors = vscode.window.visibleTextEditors;

        val = editors.find(editor => {
            let range = editor.visibleRanges[0];
            let start = range.start.line;
            let end = range.end.line;

            return end - start + 1 /* TODO: + tolerance */ < editor.document.lineCount;
        }) !== undefined;
    }

    let settings = vscode.workspace.getConfiguration();

    settings.update(
        minimapOption,
        val,
        null);
}

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {
    let inspection = vscode.workspace.getConfiguration().inspect(minimapOption);

    let inspectionResult = inspection?.workspaceFolderLanguageValue
        ?? inspection?.workspaceLanguageValue
        ?? inspection?.workspaceFolderValue
        ?? inspection?.workspaceValue;

    initialMinimapOption = typeof inspectionResult !== 'boolean'
        ? undefined
        : inspectionResult;

    vscode.window.onDidChangeVisibleTextEditors(() => updateMinimap(null));
    vscode.workspace.onDidChangeTextDocument(() => updateMinimap(null));
}

// this method is called when your extension is deactivated
export function deactivate() {
    updateMinimap(initialMinimapOption);
}
